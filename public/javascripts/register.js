
var socket = io.connect('http://werty12121.redirectme.net:665/');

var loginInput = document.getElementById("login_input");
var emailInput = document.getElementById("email_input");
var passwordInput = document.getElementById("password_input");

var registerButton = document.getElementById("register_button");

registerButton.onclick = function(){
	var login = loginInput.value;
	if(login.length < 4){
		alert("Login too short");
		return;
	}
	var email = emailInput.value;
	if(email.length < 4){
		alert("E-mail too short");
		return;
	}
	if(!checkEmail(email)){
		alert("Incorrect e-mail");
	}
	var password = passwordInput.value;
	if(password.length < 4){
		alert("Password too short");
		return;
	}
	console.log("REGISTER: login: " + login + " email: " + email + " password: " + password);

	socket.emit("register", {login: login, email: email, password: password});
}

socket.on("register_response", function(data){
	console.log("register_response");
	console.log(data);
	if(data.status == "success"){
		window.location.href = "./login";
	}else{
		showPopup("Registration Failed");
	}
});