
function showPopup(text){
	var popup = document.createElement("div");
	popup.id = "popup";
	popup.style.position = "absolute";
	popup.style.width = "200px";
	popup.style.height = "40px";
	popup.style.border = "1px solid black";
	popup.style.textAlign = "center";
	popup.style.backgroundColor = "white";
	popup.style.fontSize = "20px";
	popup.style.left = (window.innerWidth / 2 - 100) + "px";
	popup.style.top = (window.innerHeight / 4 - 20) + "px";
	popup.innerHTML = text;

	document.body.appendChild(popup);

	fade(popup);
}

function getNow(){
	return new Date().getMilliseconds();
}

function fade(element){
	var op = 1;  // initial opacity
	var timer = setInterval(function(){
		if (op <= 0.1){
			clearInterval(timer);
			document.body.removeChild(element);
		}
		element.style.opacity = op;
		element.style.filter = 'alpha(opacity=' + op * 100 + ")";
		op -= (1- op + 0.02) / 20;
	}, 50);
}

function checkEmail(text) {
	var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	return filter.test(text)
}