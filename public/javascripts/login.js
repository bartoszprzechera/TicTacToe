
var socket = io.connect('http://werty12121.redirectme.net:665/');

var loginInput = document.getElementById("login_input");
var passwordInput = document.getElementById("password_input");

var loginButton = document.getElementById("login_button");

loginButton.onclick = function(){
	var login = loginInput.value;
	if(login.length < 4){
		alert("Login too short");
		return;
	}
	var password = passwordInput.value;
	if(password.length < 4){
		alert("Password too short");
		return;
	}
	console.log("LOGIN: login: " + login + " password: " + password);

	socket.emit("login", {"login": login, "password": password});
}

socket.on("login_response", function(data){
	console.log("login_response");
	console.log(data);
	var session = data.session;
	if(session == "failed"){
		showPopup("Login Failed");
		return;
	}
	if(session == "error"){
		showPopup("Login Error");
		return;
	}
	setCookie("session", session, 2);
	window.location.href = "./connect";
});