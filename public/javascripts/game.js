
var socket = io.connect('http://werty12121.redirectme.net:665/');

var gameWrapper = document.getElementById("game_wrapper");
var gameStatus = document.getElementById("game_status");
var canvas = document.getElementById("game_canvas");
var leaveButton = document.getElementById("leave_button");

var session = getCookie("session");
var roomName = getCookie("room_name");

if(session.length == 0){
	window.location.href = "./login";
}
if(roomName.length == 0){
	window.location.href = "./connect";
}

window.onbeforeunload = endGame;

socket.emit("join_socket_to_room", {room_name: roomName});

socket.on("join_socket_to_room_response", function(data){
	console.log("join_socket_to_room_response");
	console.log(data);
	if(data.board != "error"){
		loadTable(data.board)
	}
});

socket.on("new_player_joined", function(){
	showPopup("Other Player Connected");
});

var player = getCookie("player");
if(player == 1){
	socket.on("new_player_join", function(data){
		console.log("new_player_join");
		console.log(data);
	});
}

window.onresize = resizeCanvas;

function resizeCanvas(){
	var left = Math.floor(window.innerWidth / 2 - gameWrapper.clientWidth / 2);
	var top = Math.floor(window.innerHeight / 2 - gameWrapper.clientHeight / 2);

	gameWrapper.style.left = left + "px";
	gameWrapper.style.top = top + "px";
}

canvas.onclick = function(e){
	if(player1won || player2won || draw) return;

	var rect = canvas.getBoundingClientRect();
	var mouseX = e.clientX - rect.left;
	var mouseY = e.clientY - rect.top;
	var row = Math.floor(mouseX / 100);
	var column = Math.floor(mouseY / 100);
	console.log(row + ":" + column);

	socket.emit("move", {session: session, room_name: roomName, row: row, column: column});
}

var player1won = false;
var player2won = false;
var draw = false;

socket.on("move_response", function(data){
	console.log("move_response");
	console.log(data);
	if(data.board != "error"){
		loadTable(data.board)
		if(data.win == "1"){
			player1won = true;
		}
		if(data.win == "2"){
			player2won = true;
		}
		if(data.win == "draw"){
			draw = true;
		}
	}
});

var ctx = canvas.getContext("2d");

canvas.oncontextmenu = function(){
	return false;
}

leaveButton.onclick = endGame;

function endGame(){
	socket.emit("end_game", {room_name: roomName});
	deleteCookie("room_name");
	deleteCookie("player");
	window.location.href = "./connect";
}

socket.on("end_game_response", function(data){
	console.log("end_game_response");
	showPopup("User disconnected");
	window.location.href = "./connect";
});

const EMPTY = 0;
const CROSS = 1;
const CIRCLE = 2;

var boardWidth = 3;
var boardHeight = 3;

function loadTable(board){
	this.board = board;
	boardWidth = board.length;
	boardHeight = board[0].length;
	canvas.width = boardWidth * 100;
	canvas.height = boardHeight * 100;
	resizeCanvas();
}

var board = getTable2D(boardWidth, boardHeight);

/*
socket.on("board", function(data){
	console.log("board");
	console.log(data.board);
});
*/

//console.log(board);

function getTable2D(width, height){
	var tab = [];
	for(var x = 0; x < width; x++){
		tab[x] = new Array(height);
	}
	return tab;
}

function drawLine(x1, y1, x2, y2){
	ctx.beginPath();
	ctx.moveTo(x1, y1);
	ctx.lineTo(x2, y2);
	ctx.stroke();
}

function drawCross(x, y){
	x *= 100;
	y *= 100;
	drawLine(x + 10		, y + 10, x + 80 + 10	, y + 80 + 10)
	drawLine(x + 80 + 10, y + 10, x + 10		, y + 80 + 10)
}

function drawCircle(x, y){
	x *= 100;
	y *= 100;
	ctx.beginPath();
	ctx.arc(x + 50, y + 50, 40, 0, Math.PI * 2);
	ctx.stroke();
}

function render(){
	ctx.fillStyle = "#FFFFFF";
	ctx.clearRect(0, 0, canvas.width, canvas.height);

	ctx.strokeStyle = "#000000";
	for(var i = 1; i < boardWidth; i++)
		drawLine(100 * i, 0, 100 * i, 100 * boardHeight);

	for(var i = 1; i < boardHeight; i++)
		drawLine(0, 100 * i, 100 * boardWidth, 100 * i);

	for(var y = 0; y < boardHeight; y++){
		for(var x = 0; x < boardWidth; x++){
			if(board[x][y] == CROSS){
				drawCross(x, y);
			}else if(board[x][y] == CIRCLE){
				drawCircle(x, y);
			}
		}
	}

	renderGameStatus();

	requestAnimationFrame(render);
}

function renderGameStatus(){
	ctx.strokeStyle = "#000000";
	ctx.fillStyle = "#FF0000";
	ctx.font = "42px Arial";
	var text = "";
	if(player1won){
		text = "PLAYER 1 WON";
	}
	if(player2won){
		text = "PLAYER 2 WON";
	}
	if(draw){
		text = "DRAW";
	}
	if(text != ""){
		var x = canvas.width / 2 - ctx.measureText(text).width / 2;
		var y = canvas.height / 2;
		ctx.fillText(text, x, y);
		ctx.strokeText(text, x, y);
	}
}

/*
setInterval(function(){
	console.log("blaaa");
	for(var y = 0; y < boardHeight; y++){
		for(var x = 0; x < boardWidth; x++){
			board[x][y] = Math.floor(Math.random() * 3);
		}
	}
}, 1000);
*/

render();